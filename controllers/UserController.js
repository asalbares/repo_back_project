const crypt = require ('../crypt');

// Incluimos librería de MongoDB
const requestJson = require ('request-json');
const request = require('request');

// URL base para hacer las peticiones
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edasa/collections/";
// Defino una constante para almacenar mi APIKey
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Creamos una nueva función para probar acceso a MongoDB
// La función recupera todos los usuarios de la BD
function getUsersV2 (req, res){
  console.log ("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

  // Especificamos el método http con el que realizamos la petición
  // Devolvemos con la query todos los usuarios
  httpClient.get("user?" + mlabAPIKey,
    function(err, resMLab, body){
      console.log ("Dentro API");
      var response = !err ? // Si OK, muestro el resultado de la query, si no, muestro mensaje de error
        body : {"msg" : "Error obteniendo usuarios"}

      res.send(response);
    }
  )
}
module.exports.getUsersV2 = getUsersV2;

// Creamos otra  función para recoger un user por su ID de MongoDB
function getUserByIdV2 (req, res){
  console.log ("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

  // Especificamos el método http con el que realizamos la petición
  // Devolvemos con la query el usuario que yo haya pasado por parámetro

  httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){

      if (err){
        var response = {"msg" : "Error obteniendo usuario"
      };
        res.status (500); // Error del servidor
      }else{
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario
          var response = body [0];
        }else{
          var response = {"msg" : "Usuario no encontrado"
        };
        res.status (404);
        }
      }

      // var response = !err ? // Si OK, muestro el resultado de la query, si no, muestro mensaje de error
      //  body : {"msg" : "Error obteniendo usuario"}
      //
      res.send(response);
    }
  )
}
module.exports.getUserByIdV2 = getUserByIdV2;


function createUsersV2 (req, res){
  console.log ("POST /apitechu/v2/users");
  console.log (req.body.id);
  console.log (req.body.first_name);
  console.log (req.body.last_name);
  console.log (req.body.email);
  console.log (req.body.password);

  // Recoge los valores pasados en el body para crear un nuevo registro en la base de datos
  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password) // Para hashear la password
  };

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

  httpClient.post("user?" + mlabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario guardado con éxito");
      res.status(201); //Ese estado significa creado
      res.send({"msg" : "Usuario creado con éxito"});
    }
  )
}
module.exports.createUsersV2 = createUsersV2;

function createJSONBANKuser (req, res){
  console.log ("POST /JSONBANK/user");
  console.log (req.body.first_name);
  console.log (req.body.last_name);
  console.log (req.body.email);
  console.log (req.body.password);

  // Recoge los valores pasados en el body para crear un nuevo registro en la base de datos
  var newUser = {
    "id" : req.body.email,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password) // Para hashear la password
  };

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

  httpClient.post("user?" + mlabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario guardado con éxito");
      res.status(201); //Ese estado significa creado
      res.send({"msg" : "Usuario creado con éxito"});
    }
  )
}
module.exports.createJSONBANKuser = createJSONBANKuser;
