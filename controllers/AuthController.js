const crypt = require ('../crypt');

// Incluimos librería de MongoDB
const requestJson = require ('request-json');

// URL base para hacer las peticiones
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edasa/collections/";
// Defino una constante para almacenar mi APIKey
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Función de login para MongoDB
function JSONBANKlogin(req, res){
  console.log("POST /JSONBANK/login");

  // Muestra los parámetros pasados en el BODY OJO

  var hashed_password = crypt.hash(req.body.password); // Para hashear la password
  var unhashed_password = crypt.checkpassword(req.body.password, hashed_password); // Para unhashear la password, unhashed_password vale true si la password real es igual a la hasheada

  var email = req.body.email;
  var password = req.body.password;

  var query = 'q={"email":"' + email + '"}';

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

    httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg" : "Error obteniendo usuario"};
        res.status (500); // Error del servidor
        res.send(response);
      }else{
        console.log(body.email);
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario
          var response = body [0].email;

          if (unhashed_password){
            console.log ("Usuario logado");
            var putBody = '{"$set":{"logged":true}}'; // Aquí actualizo el flag de logged

            httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT){
                console.log ("logged = true");
                var response = {
                  "msg " : "Usuario logado con éxito",
                  "idUsuario" : body[0].id};
                res.status (200);
                res.send(response);
              }
            )
          }else{
            console.log ("Password incorrecta");
            var response = {"msg" : "Password incorrecta"};
            res.status (401); //usuario no autorizado
            res.send(response);
          }
        }else{
          var response = {"msg" : "Usuario no encontrado"};
          res.status (404); //usuario no existe en la base de datos
          res.send(response);
        }
      }
      console.log (response);
    }
  )
}
module.exports.JSONBANKlogin = JSONBANKlogin;

// Función de logout en MongoDB
function JSONBANKlogout(req, res){
  // Por lo general en las funciones pondremos un console.log para saber donde estamos
  console.log("POST /JSONBANKlogout");

  // Muestra los parámetros pasados
  console.log ("Parámetros");
  console.log (req.body.email);

  var email = req.body.email;

  //var query = 'q={"email":"' + "wlycett6@java.com" + '"}';
  var query = 'q={"email": "' + req.body.email + '"}';
  //var query = 'q={"id": ' + id + '}'; //Query para un dato numerico

  //var query = 'q={"email":"' + email + '"}'; //Query para un dato string

  var httpClient = requestJson.createClient (mlabBaseURL);

    httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg" : "Error obteniendo usuario"};
        res.status (500); // Error del servidor
        res.send(response);
      }else{
        console.log (body);
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario
          var response = body [0].email;
          console.log (body[0].email); //Muestro el email del usuario encontrado

          var putBody = '{"$unset":{"logged":""}}'; // Aquí borro el flag de logged

          httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body){
            }
          )
          console.log ("Usuario deslogado");

          var response = {
            "msg " : "Usuario deslogado con éxito",
            "emailUsuario" : body[0].email};
          res.status (200);
          res.send(response);
        }else{
          console.log ("No encontrado");
          var response = {"msg" : "Usuario no encontrado"};
          res.status (404);
          res.send(response);
        }
      }
    }
  )

}
module.exports.JSONBANKlogout = JSONBANKlogout;
