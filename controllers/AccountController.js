const crypt = require ('../crypt');

// Incluimos librería de MongoDB
const requestJson = require ('request-json');
const request = require('request');

// URL base para hacer las peticiones
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edasa/collections/";
// Defino una constante para almacenar mi APIKey
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Función de login para MongoDB
function getAccounts(req, res){
  console.log("GET /JSONBANK/accounts");

  // Muestra los parámetros pasados en el BODY OJO
  console.log ("Params");
  //TESTconsole.log ("id: " + req.params.userId);
  console.log ("email: " + req.body.email);
  //console.log ("email: " + "nadir@nadir.com");

  //TESTvar userId = req.params.userId;
  var email = req.body.email;
  //var email = "nadir@nadir.com";

  //TESTvar query = 'q={"userId": ' + userId + '}'; //Query para un dato numerico

  //var query = 'q={"email": "' + req.body.email + '"}';
  var query = 'q={"email": "' + email + '"}';

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

    httpClient.get("accounts?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg" : "Error obteniendo cuenta"};
        res.status (500); // Error del servidor
        res.send(response);
      }else{
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario

        var response = body;
          res.send(response);
        }else{
          var response = {"msg" : "Cuentas no encontradas"};
          res.status (404); //usuario no existe en la base de datos
          res.send(response);
        }
      }
      console.log (response);
    }
  )
}
module.exports.getAccounts = getAccounts;

// Función de recuperar movimientos de cuentas de un usuario
function getFinancials(req, res){
  console.log("GET /JSONBANK/financials");

  // Muestra los parámetros pasados en el BODY OJO
  console.log ("Params");
  //TESTconsole.log ("id: " + req.params.userId);
  console.log ("email: " + req.body.email);
  //console.log ("email: " + "nadir@nadir.com");

  var email = req.body.email;

  var query = 'q={"email": "' + email + '"}';

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

    httpClient.get("financial?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg" : "Error obteniendo cuenta"};
        res.status (500); // Error del servidor
        res.send(response);
      }else{
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario
        var response = body;
          res.send(response);
        }else{
          var response = {"msg" : "Cuentas no encontradas"};
          res.status (404); //usuario no existe en la base de datos
          res.send(response);
        }
      }
      console.log (response);
    }
  )
}
module.exports.getFinancials = getFinancials;


// Create financial transaction W or D
function recordTransaction (req, res){
  console.log ("POST /JSONBANK/transaction");
  console.log (req.body.iban);
  console.log (req.body.amount);
  console.log (req.body.email);

  // Recoge los valores pasados en el body para crear un nuevo registro en la base de datos
  var newTransaction = {
    "email" : req.body.email,
    "IBAN" : req.body.iban,
    "transaction" : req.body.transaction,
    "amount" : req.body.amount
  };

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Transaction created");

  var query = 'q={"IBAN": "' + newTransaction.IBAN + '"}';
  httpClient.get("accounts?" + query + "&" + mlabAPIKey,
    function(err, resMLab, bodyGET){
      if(err){
        console.log ("IBAN not found");
        var response = {
          "msg" : "IBAN not found"
        }
        res.status(401);
        res.send (response);
      }else{
        httpClient.post("financial?" + mlabAPIKey, newTransaction,
          function(err, resMLab, body){
            console.log("Transacción guardada con éxito");
            console.log (newTransaction.IBAN); //Es OK
            //Graba en la collection accounts el valor del nuevo saldo
            //Busca en accounts el IBAN y obtén su amount/balance
            //Actualiza el balance con la suma o resta de la transacción
            //Escribe el resultado en accounts
            var query = 'q={"IBAN": "' + newTransaction.IBAN + '"}';
            httpClient.get("accounts?" + query + "&" + mlabAPIKey,
              function(err, resMLab, bodyBalance){
                console.log(bodyBalance.length);
                if (bodyBalance.length==0){
                  console.log("IBAN not found");
                  var response = {
                    "msg" : "IBAN not found"
                  }
                  res.send (response);
                }else{
                        var newBalance = {
                          "newValue" : parseFloat(bodyBalance[0].balance) + parseFloat(req.body.amount)
                        };

                        console.log ("Balance " + newBalance.newValue); //OK

                    var queryAccount = 'q={"IBAN": "' + newTransaction.IBAN + '"}';
                    //var putBody = '{"$set":{"balance": "' + parseFloat(newBalance.newValue) + '"}}'; //OK pero lo almacena en formato string
                    var putBody = '{"$set":{"balance": "' + newBalance.newValue + '"}}'; //OK pero lo almacena en formato string
                    //var putBody = '{"$set":{"balance": " + newBalance.newValue + "}}';
                    //var putBody = '{"$set":{"balance": "' + body[0].balance + req.body.amount + '"}}';
                    httpClient.put("accounts?" + queryAccount + "&" + mlabAPIKey, JSON.parse(putBody),
                      function (errPUT, resMLabPUT, bodyPUT){
                        console.log ("PUT actualizado");
                        var response = {
                          "msg" : "Balance actualizado"
                        }
                        res.send (response);
                      }
                    )
                  }
                  }
                )
          }
        )
      }
    }
  )
}
module.exports.recordTransaction = recordTransaction;

// Create open a new account
function openAccount (req, res){
  console.log ("POST /JSONBANK/openaccount");

  // Recoge los valores pasados en el body para crear un nuevo registro en la base de datos
  var newTransaction = {
    "email" : req.body.email,
    "IBAN" : req.body.iban,
    "userId" : req.body.transaction,
    "balance" : req.body.amount
  };

  var busy = false;
  var query = 'q={"busy" : ' + busy + '}';

  var httpClient = requestJson.createClient (mlabBaseURL);

  httpClient.get("IBANtbl?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg" : "Error getting a free IBAN"};
        res.status (500); // Error del servidor
        res.send(response);
      }else{
        console.log(body[0].IBAN);
        console.log("EMAIL " + req.body.email);
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario
          var response = body [0].IBAN;
          var putBody = '{"$set":{"busy":true}}'; // Aquí actualizo el flag de IBAN busy
          var queryIBAN = 'q={"IBAN": "' + body[0].IBAN + '"}';

          httpClient.put("IBANtbl?" + queryIBAN + "&" + mlabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT){
              console.log ("IBAN busy = true");
              console.log ("Email to be recorded on account table : " + req.body.email);
                // Recoge los valores pasados en el body para crear un nuevo registro en la base de datos

                var newAccount = {
                  "email" : req.body.email,
                  "IBAN" : body[0].IBAN,
                  "userId" : 0,
                  "balance" : "0"
                };

                var httpClient = requestJson.createClient (mlabBaseURL);

                httpClient.post("accounts?" + mlabAPIKey, newAccount,
                  function(err, resMLab, body){
                    console.log("Account successfully initiated");
                    res.status(201); //Ese estado significa creado
                    res.send({"msg" : "Account successfully initiated"});
                  }
                )
                // Fin de creación de la nueva cuenta
            }
          )

          }else{
            console.log ("No free IBAN found");
            var response = {"msg" : "No free IBAN found"};
            res.status (404);
            res.send(response);
          }
        }
      console.log (response);
    }
  )
}
module.exports.openAccount = openAccount;


// Cancel an account
function cancelAccount(req, res){
  console.log("POST /JSONBANK/cancelAccount");
  var iban = req.body.iban;
  var query = 'q={"IBAN":"' + iban + '"}'; //Defines query to find IBAN in accounts collection
  var httpClient = requestJson.createClient (mlabBaseURL);

  //var putBody = {
  //  "email" : "",
  //  "IBAN" : "",
  //  "userId" : "",
  //  "balance" : ""
  //};

  var putBody = {};

  //Leave all fields of document blank
  httpClient.put("accounts?" + query + "&" + mlabAPIKey, putBody,
    function(errPUT, resMLabPUT, bodyPUT){
      console.log ("erased account = true");

      //httpClient.post("financial?" + query + "&" + mlabAPIKey, putBody,
      //  function(errPUT, resMLabPUT, bodyPUT){
      //    console.log ("erased financials = true");

      //  var response = {
      //    "msg " : "Account and financials successfully deleted"
      //  };
      //  res.status (200);
      //  res.send(response);
      //}
    //)
    var response = {
      "msg " : "Account and financials successfully deleted"
    };
    res.status (200);
    res.send(response);
  }
  )
}
module.exports.cancelAccount = cancelAccount;


//Function get external API data
function getExternalAPI (req, res){
  console.log ("GET /JSONBANK/getexternalapi");
  apiKEY="5eb66eda1ee61406ce69f21bee0a8c72";
  //apiURL="http://api.openweathermap.org/data/2.5/weather?q='Madrid'&appid=${apiKEY}";
  apiURL="http://api.openweathermap.org/data/2.5/weather?q=Madrid&units=imperial&appid=5eb66eda1ee61406ce69f21bee0a8c72";

  request(apiURL, function (err, response, body) {
    if(err){
      console.log('error:', error);
    } else {
      res.status(200);
      res.send(body);
    }
  });
}
module.exports.getExternalAPI = getExternalAPI;
