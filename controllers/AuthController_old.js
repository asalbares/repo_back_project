const crypt = require ('../crypt');

// Incluimos librería de MongoDB
const requestJson = require ('request-json');

// URL base para hacer las peticiones
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edasa/collections/";
// Defino una constante para almacenar mi APIKey
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Función de login para MongoDB
function loginUserV2(req, res){
  // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
  console.log("POST /apitechu/v2/login");

  // Muestra los parámetros pasados
  console.log ("Parámetros");
  console.log (req.body.email);
  console.log (req.body.password);

  var hashed_password = crypt.hash(req.body.password); // Para hashear la password
  var unhashed_password = crypt.checkpassword(req.body.password, hashed_password); // Para unhashear la password, unhashed_password vale true si la password real es igual a la hasheada
  console.log (unhashed_password);
  console.log ("Password hashed");
  console.log (hashed_password);

  var email = req.body.email;
  var password = req.body.password;

  var query = 'q={"email":"' + email + '"}';

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

    httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg" : "Error obteniendo usuario"
      };
        res.status (500); // Error del servidor
      }else{
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario
          var response = body [0].email;
          console.log (body[0].email); //Muestro el email del usuario encontrado
          console.log (body[0].password); //Muestro la password del usuario encontrado
          //if (body[0].password==req.params.password){ //Si la password enviada por parámetro es igual a la de la BD, me logo
          if (crypt.checkpassword(body[0].password, hashed_password)){
            console.log ("Usuario logado");

            var putBody = '{"$set":{"logged":true}}'; // Aquí actualizo el flag de logged

            httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT){
                console.log ("PUT done");
                var response = {
                  "msg " : "Usuario logado con éxito",
                  "idUsuario" : body[0].id
                }
                res.send(response);
              }
            )
          }else{
            console.log ("Password incorrecta");
            var response = {"msg" : "Password incorrecta"};
            res.status (401); //usuario no autorizado
            res.send(response);
          }
        }else{
          var response = {"msg" : "Usuario no encontrado"};
          res.status (404);
          res.send(response);
        }
      }

      console.log (response);

    }
  )

}
module.exports.loginUserV2 = loginUserV2;

// Función de logout en MongoDB
function logoutUserV2(req, res){
  // Por lo general en las funciones pondremos un console.log para saber donde estamos
  console.log("POST /apitechu/v2/logout/:email");

  // Muestra los parámetros pasados
  console.log ("Parámetros");
  console.log (req.params.email);

  var email = req.params.email;

  var query = 'q={"email":"' + email + '"}';

  var httpClient = requestJson.createClient (mlabBaseURL);
  console.log ("Client created");

    httpClient.get("user?" + query + "&" + mlabAPIKey,
    function(err, resMLab, body){
      if (err){
        var response = {"msg" : "Error obteniendo usuario"
      };
        res.status (500); // Error del servidor
      }else{
        if (body.length > 0){ // Si el body tiene algo, entendemos que se ha encontrado el usuario
          var response = body [0].email;
          console.log (body[0].email); //Muestro el email del usuario encontrado

          var putBody = '{"$unset":{"logged":""}}'; // Aquí borro el flag de logged

          httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body){
            }
          )
          console.log ("Usuario deslogado");
        }else{
          var response = {"msg" : "Usuario no encontrado"
        };
        res.status (404);
        }
      }

      res.send(response);
    }
  )

}
module.exports.logoutUserV2 = logoutUserV2;



//function loginUserV1(req, res){
  // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
//  console.log("POST /apitechu/v1/login");

  // Vuelca en el objeto users el contenido del fichero users4.json
//  var users = require ("../users4.json");

  // Muestra los parámetros pasados
//  console.log ("Parámetros");
//  console.log (req.body);

  // Encontrar el registro del array según el parámetro usando findIndex
//  indexEncontrado = users.findIndex (img => img.email == req.body.email);

//  if (indexEncontrado > -1){
    // console.log ("Lo he encontrado en la posición " + indexEncontrado);
//    if (users[indexEncontrado].password==req.body.password){
//      console.log ("Password correcta");
//      users[indexEncontrado].logged="True";
//      console.log (users[indexEncontrado]);
//      io.writeUserDataToFile(users, "./users4.json");
//      res.send ("Mensaje : Ud está logado");
//    }else{
//      console.log ("Password incorrecta");
//      res.send ("Mensaje : Password incorrecta " + "Usuario ID " + users[indexEncontrado].id);
//    }
//  }else{
//    console.log ("No he encontrado el email");
//    res.send ("Mensaje : Login incorrecto");
//  }
//}
//module.exports.loginUserV1 = loginUserV1;


// req representa la petición http, y res representa la respuesta
// function logoutUserV1(req, res){
  // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
//  console.log("POST /apitechu/v1/logout");

  // Vuelca en el objeto users el contenido del fichero users4.json
//  var users = require ("../users4.json");

  // Muestra los parámetros pasados
//  console.log ("Parámetros");
//  console.log (req.body);

  // Encontrar el registro del array según el parámetro usando findIndex
//  indexEncontrado = users.findIndex (img => img.id == req.body.id);

//  if (indexEncontrado > -1){
    // console.log ("Lo he encontrado en la posición " + indexEncontrado);
//    if (users[indexEncontrado].logged=="True"){
//      console.log ("Logout correcto");
      // Con el siguiente comando elimino la propiedad
//      delete users[indexEncontrado].logged;
      //users[indexEncontrado].logged="False";
//      console.log (users[indexEncontrado]);
//      io.writeUserDataToFile(users, "./users4.json");
//      res.send ("Mensaje : Ud está deslogado");
//    }else{
//      console.log ("ID incorrecto");
//      res.send ("Mensaje : ID incorrecto ");
//    }
//  }else{
//    console.log ("No he encontrado el ID");
//    res.send ("Mensaje : No he encontrado ID");
//  }
//}
// module.exports.logoutUserV1 = logoutUserV1;
