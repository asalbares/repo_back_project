// Constantes básicas
require('dotenv').config();
const express = require ('express');
const request = require('request');
const app = express();
app.use(express.json());

// Incluimos esto para permitir la petición AJAX
var enableCORS = function(req, res, next) {
console.log("enableCORS");

res.set("Access-Control-Allow-Origin", "*");
res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

res.set("Access-Control-Allow-Headers", "Content-Type");

next();
}
app.use(enableCORS);

// Incluimos controladores
const userController = require ('./controllers/UserController');
const AuthController = require ('./controllers/AuthController');
const AccountController = require ('./controllers/AccountController');

// Defino un puerto
const port = process.env.PORT || 3000;
// Prepara el puerto para recibir peticiones en el servidor, así le dice al servidor que empiece a escuchar
app.listen(port);

console.log ("API escuchando en el puerto " + port);

// Función para recuperar todos los usuarios de la BD
app.get("/apitechu/v2/users", userController.getUsersV2);
// Función para recuperar un usuario de la BD por ID
app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);
//app.get("/apitechu/v2/accounts/:userId", AccountController.getAccounts);

// Functions already ready for my JSONBANK project
app.post("/JSONBANK/login", AuthController.JSONBANKlogin);
app.post("/JSONBANK/user", userController.createJSONBANKuser);
app.post("/JSONBANK/logout", AuthController.JSONBANKlogout);
app.post("/JSONBANK/accounts", AccountController.getAccounts);
app.post("/JSONBANK/financials", AccountController.getFinancials);
app.post("/JSONBANK/transaction", AccountController.recordTransaction);
app.post("/JSONBANK/newaccount", AccountController.openAccount);
app.post("/JSONBANK/cancelaccount", AccountController.cancelAccount);
app.get("/JSONBANK/getexternalapi", AccountController.getExternalAPI);
